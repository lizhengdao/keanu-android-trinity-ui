package info.guardianproject.keanuapp.tasks;

import android.os.AsyncTask;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

import info.guardianproject.keanu.core.model.ImErrorInfo;
import info.guardianproject.keanu.core.model.impl.BaseAddress;
import info.guardianproject.keanu.core.service.IImConnection;
import info.guardianproject.keanu.core.service.RemoteImService;
import info.guardianproject.keanu.core.model.Address;

import static info.guardianproject.keanu.core.KeanuConstants.LOG_TAG;

/**
 * Created by n8fr8 on 6/9/15.
 */
public class AddContactAsyncTask extends AsyncTask<String, Void, Integer> {

    long mProviderId;
    long mAccountId;

    public AddContactAsyncTask(long providerId, long accountId)
    {
        mProviderId = providerId;
        mAccountId = accountId;

    }

    @Override
    public Integer doInBackground(String... strings) {

        String address = strings[0];
        String nickname = new BaseAddress(address).getUser();

        return addToContactList(address, nickname);
    }

    @Override
    protected void onPostExecute(Integer response) {
        super.onPostExecute(response);

    }

    private int addToContactList (String address, String nickname)
    {
        int res = -1;

        try {
            IImConnection conn = RemoteImService.getConnection(mProviderId,mAccountId);
            if (conn == null)
               conn = RemoteImService.createConnection(mProviderId,mAccountId);

            /**
            IContactList list = getContactList(conn);

            if (list != null) {

                    res = list.addContact(address, nickname);
                    if (res != ImErrorInfo.NO_ERROR) {

                        //what to do here?
                    }

            }**/

        } catch (RemoteException re) {
            Log.e(LOG_TAG, "error adding contact", re);
        }

        return res;
    }


}
