package info.guardianproject.keanu.core;

import android.content.Context;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import info.guardianproject.keanu.core.model.Address;
import info.guardianproject.keanu.core.model.ChatGroup;
import info.guardianproject.keanu.core.model.ChatGroupManager;
import info.guardianproject.keanu.core.model.ChatSession;
import info.guardianproject.keanu.core.model.ChatSessionListener;
import info.guardianproject.keanu.core.model.ChatSessionManager;
import info.guardianproject.keanu.core.model.ConnectionListener;
import info.guardianproject.keanu.core.model.Contact;
import info.guardianproject.keanu.core.model.ContactList;
import info.guardianproject.keanu.core.model.ContactListManager;
import info.guardianproject.keanu.core.model.ImConnection;
import info.guardianproject.keanu.core.model.ImException;
import info.guardianproject.keanu.core.model.Invitation;
import info.guardianproject.keanu.core.model.Message;
import info.guardianproject.keanu.core.model.Presence;
import info.guardianproject.keanu.core.model.impl.BaseAddress;
import info.guardianproject.keanu.core.service.IChatSessionListener;

public class DummyImConnection extends ImConnection {

    protected DummyImConnection(Context context) {
        super(context);
    }

    @Override
    public Contact getLoginUser() {
        return new Contact (new BaseAddress("@hello:you.com"));
    }

    @Override
    public int[] getSupportedPresenceStatus() {
        return new int[0];
    }

    @Override
    public void initUser(long providerId, long accountId) throws ImException {

    }

    @Override
    public int getCapability() {
        return 0;
    }

    @Override
    public void loginAsync(long accountId, String passwordTemp, long providerId, boolean retry) {

    }

    @Override
    public void reestablishSessionAsync(Map<String, String> sessionContext) {

    }

    @Override
    public void logoutAsync() {

    }

    @Override
    public void logout(boolean fullLogout) {

    }

    @Override
    public void suspend() {

    }

    @Override
    public Map<String, String> getSessionContext() {
        return null;
    }

    @Override
    public ChatSessionManager getChatSessionManager() {
        return mChatSessionManager;
    }

    @Override
    public ChatGroupManager getChatGroupManager() {
        return mChatGroupManager;
    }

    @Override
    public boolean isUsingTor() {
        return false;
    }

    @Override
    protected void doUpdateUserPresenceAsync(Presence presence) {

    }

    @Override
    public void sendHeartbeat(long heartbeatInterval) {

    }

    @Override
    public void setProxy(String type, String host, int port) {

    }

    @Override
    public void sendTypingStatus(String to, boolean isTyping) {

    }

    @Override
    public void sendMessageRead(String to, String msgId) {

    }

    @Override
    public List<String> getFingerprints(String address) {
        ArrayList<String> keyList = new ArrayList<>();
        keyList.add("12345");
        return keyList;
    }

    @Override
    public void setDeviceVerified(String address, String device, boolean verified) {

    }

    @Override
    public void broadcastMigrationIdentity(String newIdentity) {

    }

    @Override
    public void changeNickname(String nickname) {

    }

    @Override
    public void searchForUser(String searchString) {

    }

    @Override
    public void uploadContent(InputStream is, String contentTitle, String mimeType, ConnectionListener listener) {

    }

    @Override
    public String getDownloadUrl(String identifier) {
        return "https://foo.com/1234";
    }

    @Override
    public void syncMessages(ChatSession session) {

    }

    @Override
    public void refreshMessage(ChatSession session, String msgId) {

    }

    @Override
    public void checkReceipt(ChatSession session, String msgId) {

    }

    ChatSessionManager mChatSessionManager = new ChatSessionManager() {
        @Override
        public void sendMessageAsync(ChatSession session, Message message, ChatSessionListener listener) {

        }

        @Override
        public void enableEncryption(ChatSession session, boolean encryption) {

        }

        @Override
        public void setPublic(ChatSession session, boolean isPublic) {

        }

        @Override
        public String getPublicAddress(ChatSession session) {
            return "https://foo.com";
        }
    };

    ContactListManager mContactListManager = new ContactListManager() {
        @Override
        public String normalizeAddress(String address) {
            return address;
        }

        @Override
        public Contact[] createTemporaryContacts(String[] addresses) {
            return new Contact[0];
        }

        @Override
        protected void doSetContactName(String address, String name) throws ImException {

        }

        @Override
        public void loadContactListsAsync() {

        }

        @Override
        public void approveSubscriptionRequest(Contact contact) {

        }

        @Override
        public void declineSubscriptionRequest(Contact contact) {

        }

        @Override
        protected ImConnection getConnection() {
            return DummyImConnection.this;
        }

        @Override
        protected void doBlockContactAsync(String address, boolean block) {

        }

        @Override
        protected void doCreateContactListAsync(String name, Collection<Contact> contacts, boolean isDefault) {

        }

        @Override
        protected void doDeleteContactListAsync(ContactList list) {

        }

        @Override
        protected void doAddContactToListAsync(Contact contact, ContactList list, boolean autoPresenceSubscribe) throws ImException {

        }

        @Override
        protected void doRemoveContactFromListAsync(Contact contact, ContactList list) {

        }

        @Override
        protected void setListNameAsync(String name, ContactList list) {

        }
    };

    ChatGroupManager mChatGroupManager = new ChatGroupManager() {
        @Override
        public void createChatGroupAsync(String subject, boolean isDirect, boolean isEncrypted, boolean isPrivate, IChatSessionListener listener) throws Exception {

        }

        @Override
        public void deleteChatGroupAsync(ChatGroup group) {

        }

        @Override
        protected void addGroupMemberAsync(ChatGroup group, Contact contact) {

        }

        @Override
        public void removeGroupMemberAsync(ChatGroup group, Contact contact) {

        }

        @Override
        public void joinChatGroupAsync(Address address, IChatSessionListener listener) {

        }

        @Override
        public void leaveChatGroupAsync(ChatGroup group) {

        }

        @Override
        public void inviteUserAsync(ChatGroup group, Contact invitee) {

        }

        @Override
        public void acceptInvitationAsync(Invitation invitation) {

        }

        @Override
        public void rejectInvitationAsync(Invitation invitation) {

        }

        @Override
        public String getDefaultGroupChatService() {
            return null;
        }

        @Override
        public void setGroupSubject(ChatGroup group, String subject) {

        }

        @Override
        public void grantAdminAsync(ChatGroup group, Contact contact) {

        }

        @Override
        public void refreshGroup(ChatGroup group) {

        }
    };
}
