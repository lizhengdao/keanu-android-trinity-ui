package info.guardianproject.keanu.matrix;

import android.content.Context;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import info.guardianproject.keanu.core.model.ChatGroupManager;
import info.guardianproject.keanu.core.model.ChatSession;
import info.guardianproject.keanu.core.model.ChatSessionManager;
import info.guardianproject.keanu.core.model.ConnectionListener;
import info.guardianproject.keanu.core.model.Contact;
import info.guardianproject.keanu.core.model.ImConnection;
import info.guardianproject.keanu.core.model.ImException;
import info.guardianproject.keanu.core.model.Presence;

public class MatrixConnection extends ImConnection {

    public MatrixConnection(Context context) {
        super(context);
    }

    @Override
    public Contact getLoginUser() {
        return null;
    }

    @Override
    public int[] getSupportedPresenceStatus() {
        return new int[0];
    }

    @Override
    public void initUser(long providerId, long accountId) throws ImException {

    }

    @Override
    public int getCapability() {
        return 0;
    }

    @Override
    public void loginAsync(long accountId, String passwordTemp, long providerId, boolean retry) {

    }

    @Override
    public void reestablishSessionAsync(Map<String, String> sessionContext) {

    }

    @Override
    public void logoutAsync() {

    }

    @Override
    public void logout(boolean fullLogout) {

    }

    @Override
    public void suspend() {

    }

    @Override
    public Map<String, String> getSessionContext() {
        return null;
    }

    @Override
    public ChatSessionManager getChatSessionManager() {
        return null;
    }

    @Override
    public ChatGroupManager getChatGroupManager() {
        return null;
    }

    @Override
    public boolean isUsingTor() {
        return false;
    }

    @Override
    protected void doUpdateUserPresenceAsync(Presence presence) {

    }

    @Override
    public void sendHeartbeat(long heartbeatInterval) {

    }

    @Override
    public void setProxy(String type, String host, int port) {

    }

    @Override
    public void sendTypingStatus(String to, boolean isTyping) {

    }

    @Override
    public void sendMessageRead(String to, String msgId) {

    }

    @Override
    public List<String> getFingerprints(String address) {
        return null;
    }

    @Override
    public void setDeviceVerified(String address, String device, boolean verified) {

    }

    @Override
    public void broadcastMigrationIdentity(String newIdentity) {

    }

    @Override
    public void changeNickname(String nickname) {

    }

    @Override
    public void searchForUser(String searchString) {

    }

    @Override
    public void uploadContent(InputStream is, String contentTitle, String mimeType, ConnectionListener listener) {

    }

    @Override
    public String getDownloadUrl(String identifier) {
        return null;
    }

    @Override
    public void syncMessages(ChatSession session) {

    }

    @Override
    public void refreshMessage(ChatSession session, String msgId) {

    }

    @Override
    public void checkReceipt(ChatSession session, String msgId) {

    }
}
